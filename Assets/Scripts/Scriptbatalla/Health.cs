using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{
    [SerializeField] float maxHealth = 3f;
    [SerializeField] bool destroyOnDeath = true;
    [SerializeField] float destroyDelay = 1f;
    [SerializeField] UnityEvent<float> takeDamage;
    [SerializeField] UnityEvent onDie;
    private LivesHandler Lives;
    private LivesDisplay ULives;

    private CambioScenas cambio;

    private float currentHealthPoints;
    private Animator animator;
    private bool isDead = false;

    public int vidas = 3;
    public int playerLives;

    public event Action onHealthUpdated;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        currentHealthPoints = maxHealth;
    }

    private void Start()
    {
        onHealthUpdated?.Invoke();
        Lives = GetComponent<LivesHandler>();
    }

    public bool IsDead()
    {
        return isDead;
    }

    public float GetPercentage()
    {
        return 100 * GetFraction();
    }

    public float GetFraction()
    {
        return currentHealthPoints / maxHealth;
    }

    public float GetHealthPoints()
    {
        return currentHealthPoints;
    }

    /*public float GetScore(){
        return 
    }*/

    public void SetHealthPoints(float healthPoints)
    {
        currentHealthPoints = healthPoints;
        onHealthUpdated?.Invoke();
    }

    public void TakeDamage(float damage)
    {
        animator.ResetTrigger("takeDamage");
        currentHealthPoints = Mathf.Max(currentHealthPoints - damage, 0);
        takeDamage?.Invoke(damage);
        onHealthUpdated?.Invoke();
        if (currentHealthPoints == 0)
        {
            onDie?.Invoke();
            Die();
        }
        else
        {
            animator.SetTrigger("takeDamage");
        }
    }

    private void Die()
    {
               
        //Vida(vidas);
        if (isDead) return;

        isDead = true;
        animator.SetTrigger("die");
        if (destroyOnDeath)
        {
            Destroy(gameObject, destroyDelay);
            if ( vidas > 0 )
            {
                vidas = vidas - 1;     
                Debug.Log("Perdió una vida" + vidas);  
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);   

            } 
            if ( vidas <= 0) 
            {
                Debug.Log("Perdió el juego");
            }       

            
        }
    }

    public void Vida()
    {
        
        /*
        Lives.GetPlayerLives();
        vidas = playerLives;

        
        if (vidas < 3){

            vidas = vidas + 1;
        }
        if (vidas == 3){

            vidas = vidas; 
            cambio.LoadScene();

        }
        */
    }
}
