using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CambioScenas : MonoBehaviour
{
    public string SceneName;

    public void LoadScene()
    {
        SceneManager.LoadScene(SceneName);
    }

    public void Exit() 
    {
        Application.Quit();
        //EditorApplication.Exit(0);
        Debug.Log("Salió del juego");
    }
}
