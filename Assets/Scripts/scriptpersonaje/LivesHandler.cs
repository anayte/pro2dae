using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LivesHandler : MonoBehaviour
{
    
    [SerializeField] private int playerLives = 3;
    [SerializeField] float processPlayerDeathDelay = 2f;

    public event Action onLivesUpdated;
    public static event Action onGameOver;

    public int GetPlayerLives()
    {
        return playerLives;
    }

    public void SetPlayerLives(int lives)
    {
        playerLives = lives;
        onLivesUpdated?.Invoke();
    }

    public void ProcessPlayerDeath() //Invoked by Health Unity Event
    {
        StartCoroutine(ExecutePlayerDeath());
    }

    private IEnumerator ExecutePlayerDeath()
    {
        if (playerLives > 0)
        {
            StartCoroutine(TakeLife());
        }
        else
        {
            yield return new WaitForSeconds(processPlayerDeathDelay);
            onGameOver?.Invoke();
        }
    }

    public IEnumerator TakeLife()
    {
        playerLives--;
        onLivesUpdated?.Invoke();

        yield return new WaitForSeconds(processPlayerDeathDelay);
        GameSession gameSession = FindObjectOfType<GameSession>();
        yield return StartCoroutine(gameSession.SavePlayerData(false));
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
    }
    
}
