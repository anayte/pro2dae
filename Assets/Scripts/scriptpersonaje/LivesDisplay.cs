using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LivesDisplay : MonoBehaviour
{
    
    [SerializeField] private LivesHandler playerLivesHandler;
    [SerializeField] private TextMeshProUGUI livesText;

    private void Start()
    {
        HandleLivesUpdated();
    }

    private void OnEnable()
    {
        playerLivesHandler.onLivesUpdated += HandleLivesUpdated;
    }

    private void OnDisable()
    {
        playerLivesHandler.onLivesUpdated -= HandleLivesUpdated;
    }

    private void HandleLivesUpdated()
    {
        livesText.text = "Vidas " + playerLivesHandler.GetPlayerLives();
    }
    
}
