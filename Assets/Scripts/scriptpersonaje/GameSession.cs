using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSession : MonoBehaviour
{
    
    static bool hasSpawned = false;
    private GameObject playerGO;
    private float currentHealth;
    private int currentLives;
    private int currentScore;
    private bool sceneWasUnloaded = false;
    private bool saveAllPlayerData = false;

    void Awake()
    {
        if (hasSpawned)
        {
            Destroy(gameObject);
        }
        else
        {
            hasSpawned = true;
            DontDestroyOnLoad(gameObject);
        }
    }

    void Start()
    {
        Debug.Log("start del gs");
        StartCoroutine(SavePlayerData(true));
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        LivesHandler.onGameOver += ResetGameSession;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
        LivesHandler.onGameOver -= ResetGameSession;
    }
    
    public IEnumerator SavePlayerData(bool saveAllPlayerData)
    {
        this.saveAllPlayerData = saveAllPlayerData;
        playerGO = GameObject.FindGameObjectWithTag("Player");
        currentLives = playerGO.GetComponent<LivesHandler>().GetPlayerLives();
        if (saveAllPlayerData)
        {
            currentHealth = playerGO.GetComponent<Health>().GetHealthPoints();
            Debug.Log("health: " + currentHealth + " lives: " + currentLives + " player name: " + playerGO.name + " p health: " + playerGO.GetComponent<Health>().GetHealthPoints());

            //currentScore = playerGO.GetComponent<Score>().GetScore();
        }

        sceneWasUnloaded = true;

        yield return null;
    }

    private void OnSceneLoaded(Scene current, LoadSceneMode loadSceneMode)
    {
        Debug.Log("OnSceneLoaded: " + current.name);
        if (!sceneWasUnloaded) { return; }
        sceneWasUnloaded = false;
        playerGO = GameObject.FindGameObjectWithTag("Player");

        playerGO.GetComponent<LivesHandler>().SetPlayerLives(currentLives);
        playerGO.GetComponent<Health>().SetHealthPoints(currentHealth);
        //playerGO.GetComponent<Score>().SetScore(currentScore);
    }

    void ResetGameSession()
    {
        hasSpawned = false;
        SceneManager.LoadScene(0);
        Destroy(gameObject);
    }
    
}
